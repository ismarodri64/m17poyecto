﻿using UnityEngine;
using UnityEngine.UI;

public class ContenedorIU : MonoBehaviour
{
    //tenemos los textos y la opcion de vender, al pulsar de nuevo en la caja que 
    //colocamos la unidad nos saldra la opcion, si le das otra vez se esconde
    public GameObject ui;

    public Text VentaPrecio;

    private Caja tarjeta;

    public void SetTarjeta(Caja _tarjeta)
    {
        tarjeta = _tarjeta;

        transform.position = tarjeta.GetBuildPosition();

        VentaPrecio.text = "$" + tarjeta.planosCos.GetCantidadVenta();

        ui.SetActive(true);
    }

    public void Esconder()
    {
        ui.SetActive(false);
    }

    public void Mejora()
    {
        tarjeta.MejorarUnidad();
        BuildManager.instance.DeselecionarCaja();
    }

    public void Vender()
    {
        tarjeta.VentaUnidad();
        BuildManager.instance.DeselecionarCaja();
    }
}
