﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tanke : MonoBehaviour
{
    public int vida = 50;
    //Animator del Tanke
    public Animator anim;
    //Clip de audio de ataque
    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        //Asignamos Animator
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Cuando colisionamos con Orco o con Enemigo se activa el clip de audio y
        //ponemos a true el bool de la animacion
        if (collision.gameObject.tag == "Orco" || collision.gameObject.tag == "Enemigo")
        {
            AudioSource.PlayClipAtPoint(clip, this.transform.position);
            anim.SetBool("ataque", true);
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        //Al salir de cualquier colision, ponemos el Bool de la animacion en false...
        //esto tengo que afinarlo.No funciona cuando el collision muere...
        anim.SetBool("ataque", false);
    }

}
