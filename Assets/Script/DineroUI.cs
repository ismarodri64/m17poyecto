﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DineroUI : MonoBehaviour
{
    //ui con el text dinero, se actualiza segun vamos consiguiendo mas
    public Text dineroText;

     void Update()
    {
        dineroText.text = "$" + Stats.money.ToString();
    }
}
