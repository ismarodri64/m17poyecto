﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    //Velocidad de movimiento de la unidad
    [SerializeField]
    private float velocity = 1f;
    public int vida = 20;
    public int daño = 10;
    public GameManager gameManager;
    
    void Start()
    {
        //Cogemos el script GameManager
        gameManager = FindObjectOfType<GameManager>();
        //Velocidad y direccion de la unidad al instanciarse
        GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, 0f);
    }

    void Update()
    {
        TeMato();
    }

    //Funcion que activa el game over si este enemigo sale de la pantalla por la parte izq.
    private void TeMato()
    {
        if (this.transform.position.x <= -9.5f )
        {
            gameManager.GameOver();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //si colisionamos con el Tanke..
        if (collision.gameObject.tag == "Tanke")
        {
            //Quitamos vida al tanke
            collision.gameObject.GetComponent<Tanke>().vida -= daño;
            //Si la vida del tanke llega a 0, muere.
            if (collision.gameObject.GetComponent<Tanke>().vida <= 0)
            {
                Destroy(collision.gameObject);
            }
            //Tanke daña a Enemigo1
            this.vida -= 10;
            //Si vida Enemigo1 llega a 0, muere
            if (vida <= 0)
            {
                Stats.money += 40;
                Destroy(this.gameObject);
            }
            //sufrimos empujon del Tanke
            this.transform.position = new Vector2(this.transform.position.x + 2, this.transform.position.y);
            
        }
        //si colisionamos con un mago..
        if (collision.gameObject.tag == "Arquero")
        {
            //Hacemos daño al mago
            collision.gameObject.GetComponent<Arquero>().vida -= daño;
            //Y si su vida llega ha cero, lo matamos.
            if (collision.gameObject.GetComponent<Arquero>().vida <= 0)
            {
                Destroy(collision.gameObject);
            }
        }
    }
}
