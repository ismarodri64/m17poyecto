﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Tienda : MonoBehaviour
{
    //aqui pasamos al que queremos construir despues de llamarlo en la funcion de abajo que estan definidos en planosCos
    public PlanosCos arqueroUnidad;
    public PlanosCos guerreroUnidad;
    public PlanosCos minaUnidad;

    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;

    }

    //Estos metodos se llaman cuando hacemos click en algo 
    public void SelectArquenoUnidad()
    {
        
        Debug.Log("Selecion de la Unidad Arquero");
        buildManager.SelectUnidadToBuild(arqueroUnidad);
        
    }
    
    public void SelectGuerreroUnidad()
    {
        Debug.Log("Selecion de la Unidad Guerrero");
        buildManager.SelectUnidadToBuild(guerreroUnidad);
    }

    public void SelectMinaUnidad()
    {
        Debug.Log("Selecion de la Unidad Guerrero");
        buildManager.SelectUnidadToBuild(minaUnidad);
    }

    
}

