﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Volador : MonoBehaviour
{
    //Velocidad de esta unidad
    [SerializeField]
    private float velocity = 1.5f;
    public int vida = 10;
    public int daño = 10;
    public GameManager gameManager;
    //Animator de esta unidad
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        //Asignamos animator
        anim = GetComponent<Animator>();
        //Asignamos el script GameManager
        gameManager = FindObjectOfType<GameManager>();
        //Fijamos velocidad y direccion de la unidad
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        TeMato();
    }
    //Funcion que activa el game over si este enemigo sale de la pantalla por la parte izq.
    private void TeMato()
    {
        if (this.transform.position.x <= -9.5f)
        {
            gameManager.GameOver();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //si colisionamos con un Mago..
        if (collision.gameObject.tag == "Arquero")
        {
            //Restamos vida al Mago
            collision.gameObject.GetComponent<Arquero>().vida -= daño;
            //Activamos animacion de ataque
            anim.SetBool("voladorAtaca", true);
            //Si el magose queda sin puntos de vida, muere
            if (collision.gameObject.GetComponent<Arquero>().vida <= 0)
            {
                Destroy(collision.gameObject);
            }
        }  
    }
    //Al salir de la colision, desactivamos bool y volvemos a la animacion de volar
    private void OnTriggerExit2D(Collider2D collision)
    {
        anim.SetBool("voladorAtaca", false);
    }
}
