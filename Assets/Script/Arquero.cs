﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Arquero : MonoBehaviour
{
    //Bola de ataque del mago
    public GameObject flecha;
    public int vida = 5;
    //animacion de ataque
    public Animator anim;
    //Clip de audio de lanzamiento de hechizo
    public AudioClip clip;

    private void Start()
    {
        //Asignamos animator
        anim = GetComponent<Animator>();
    }
    //Funcion de ataque del Mago. Es llamada por su rango de ataque (Impacto)
    public void Disparar()
    {
        //Activamos la animacion de ataque del mago
        anim.SetBool("ataque", true);
        //activamos el clip de audio de lanzamiento de hechizo
        AudioSource.PlayClipAtPoint(clip, this.transform.position);
        //Instanciamos una bola de mago
        Instantiate(flecha, transform.position + new Vector3(0.8f, 0, 0), Quaternion.identity);
    }
}
