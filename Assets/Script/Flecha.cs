﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Esta es la Bola de hielo del Mago
public class Flecha : MonoBehaviour
{
    //Velocidad de movimiento de la Bola de Mago
    private float velocity = 1.8f;
    private int daño = 3;
    public GameManager gameManager;
    //Audio clip de choque de la bola contra enemigos
    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        //Asignamos el GameManager
        gameManager = FindObjectOfType<GameManager>();
        //Fijamos velocidad y direccion a la bola de Mago
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        MeDestruyo();
    }

    //para que no tengamos mil bolas de hielo instanciadas hacemos que se autodestruyan al salir de la pantalla.
    private void MeDestruyo()
    {
        if (this.transform.position.x > 10)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si impactamos con un enemigo (Limo)
        if (collision.gameObject.tag == "Enemigo")
        {
            //activamos audio de colision
            AudioSource.PlayClipAtPoint(clip, this.transform.position);
            //Esta funcion se encarga de calcular si el gope es critico o no
            GolpeCritico();
            //Restamos vida al Limo
            collision.gameObject.GetComponent<Enemigo1>().vida -= daño;
            //Si la vida del Limo llega a 0, muere y sumamos puntos y dinero al marcador
            if (collision.gameObject.GetComponent<Enemigo1>().vida <= 0)
            {
                Destroy(collision.gameObject);
                //nos da puntuacion del enemigo derrotado y se actualiza
                Puntuacion.puntuacion.IncrementarPuntuacion(1);
                Stats.money += 80;
            }
            //Al impactar, la bola se destruye
            Destroy(this.gameObject);
        }
        //Si colisionamos con Orco (Caballo)
        if (collision.gameObject.tag == "Orco")
        {
            //activamos audio de colision
            AudioSource.PlayClipAtPoint(clip, this.transform.position);
            //Lo mismo que arriba...
            GolpeCritico();
            //Lo mismo que arriba...
            collision.gameObject.GetComponent<Orco>().vida -= daño;
            //Lo mismo que arriba... pero sumamos mas puntos y ganamos mas dinero
            if (collision.gameObject.GetComponent<Orco>().vida <= 0)
            {
                Destroy(collision.gameObject);
                //nos da puntuacion del enemigo derrotado y se actualiza
                Puntuacion.puntuacion.IncrementarPuntuacion(5);
                Stats.money += 100;
            }
            //Al impactar, la bola se destruye
            Destroy(this.gameObject);
        }
        //Si colisionamos con un volador..
        if (collision.gameObject.tag == "Volador1")
        {
            AudioSource.PlayClipAtPoint(clip, this.transform.position);
            GolpeCritico();
            collision.gameObject.GetComponent<Volador>().vida -= daño;

            if (collision.gameObject.GetComponent<Volador>().vida <= 0)
            {
                //anim = collision.gameObject.GetComponent<Animator>();
                //anim.SetBool("voladorMuerto", true);
                Destroy(collision.gameObject);
                //nos da puntuacion del enemigo derrotado y se actualiza
                Puntuacion.puntuacion.IncrementarPuntuacion(3);
                Stats.money += 80;
            }
            Destroy(this.gameObject);
        }
        
    }

    private void GolpeCritico()
    {
        int rangoCritico = Random.Range(1, 11);
        //20% de critico
        if (rangoCritico > 8)
        {
            daño *= 2;
        }
    }
}
