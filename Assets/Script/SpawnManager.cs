﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    //Array de prefabs de enemigos
    public GameObject[] PrefabsEnemigos;
    //Array de posiciones de los carriles del mapa
    public float[] posicionSpawn = new float[3];
    //Referencia en eje X para spawnear enemigos
    private float referenciaX = 8f;
    

    void Start()
    {
        //Relleno a manija de las posiciones de spawn de enemigos
        posicionSpawn[0] = -3.46f;
        posicionSpawn[1] = -0.46f;
        posicionSpawn[2] = 2.54f;
        //Generamos enemigos cada 4s desde el s 0
        InvokeRepeating("GenerarEnemigos", 0, 4);
    }

    public void GenerarEnemigos()
    {
        int aleatorioFila = Random.Range(0, 3);
        int aleatorioEnemigo = Random.Range(0, 2);
        int aleatorioOrco = Random.Range(1, 11);
        //20% de probabilidad de que spawnee un Orco (Caballo)
        if (aleatorioOrco > 8)
        {
            //instanciamos gameObject Orco
            GameObject nuevoOrco = Instantiate(PrefabsEnemigos[1]);
            //Y le asignamos un carril aleatorio del mapa
            nuevoOrco.transform.position = new Vector2(referenciaX, posicionSpawn[aleatorioFila]);
        }
        //40% de probabilidad de que spawnee un Volador
        else if (aleatorioOrco > 4 && aleatorioOrco < 9)
        {
            //Instanciamos gameObject volador
            GameObject nuevoVolador1 = Instantiate(PrefabsEnemigos[2]);
            //Y le asignamos un carril del mapa aleatoriamente
            nuevoVolador1.transform.position = new Vector2(referenciaX, posicionSpawn[aleatorioFila] + 0.2f);
        }
        //40% de que spawnee un Limo
        else
        {
            //Instanciamos gameObject Enemigo (Limo)
            GameObject nuevoEnemigo1 = Instantiate(PrefabsEnemigos[0]);
            //Y le asignamos un carril random del mapa
            nuevoEnemigo1.transform.position = new Vector2(referenciaX, posicionSpawn[aleatorioFila]);
        }
    }
}
