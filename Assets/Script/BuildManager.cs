﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEditor.Experimental.UIElements.GraphView;
using UnityEngine;


public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("nOOOOOOO");
            return;
        }
        instance = this;
    }


    //variable privada del objeto privado
    private PlanosCos unidadToBuild;
    private Caja selecionarCaja;

    public ContenedorIU contenedorIU;
    
    //Solo es para obtener algo, es decir, solo comprobara si unidadToBuild es igual a null,
    //o no es igual a null y nos devolvera el resultado
    public bool CanBuild { get { return unidadToBuild != null; } }
    public bool HasMoney { get { return Stats.money >= unidadToBuild.cost; } }
    
    //aqui selecionamos la caja que queramos usar
    public void SelectCaja(Caja caja)
    {
        if(selecionarCaja == caja)
        {
            DeselecionarCaja();
            return;
        }

        selecionarCaja = caja;
        unidadToBuild = null;

        contenedorIU.SetTarjeta(caja);
    }
    //guardamos la caja hasta volver a selecionarlar
    public void DeselecionarCaja()
    {
        unidadToBuild = null;
        contenedorIU.Esconder();
    }
    //selecionamos la unidad que queramos poner
    public void SelectUnidadToBuild(PlanosCos unidad)
    {
        
        unidadToBuild = unidad;
        selecionarCaja = null;

        contenedorIU.Esconder();
    }

    public PlanosCos GetUnidadToBuild()
    {
        return unidadToBuild;
    }
}
