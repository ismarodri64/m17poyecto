﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mina : MonoBehaviour
{
    public int daño = 20;
    public GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            collision.gameObject.GetComponent<Enemigo1>().vida -= daño;
            if (collision.gameObject.GetComponent<Enemigo1>().vida <= 0)
            {
                Destroy(collision.gameObject);
                Stats.money += 30;
            }
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Orco")
        {
            collision.gameObject.GetComponent<Orco>().vida -= daño;
            if (collision.gameObject.GetComponent<Orco>().vida <= 0)
            {
                Destroy(collision.gameObject);
                Stats.money += 50;
            }
            Destroy(this.gameObject);
        }
    }
}
