﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlanosCos 
{
    // basicamente es un prefab y el coste que tendra ese prefab en la tienda
    public GameObject prefab;
    public int cost;

    public GameObject mejoraPrefab;
    public int mejoracost;

    //dinero que nos dan al vender
    public int GetCantidadVenta()
    {
        return cost / 2;
    }
}
