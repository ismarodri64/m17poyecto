﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



// Aqui basicamente estamos diciendolo en que caja lo queremos construir y si esta todo bien para hacerlo
public class Caja : MonoBehaviour
{
    //color de la caja al tener o no dinero
    public Color hoverColor;
    public Color noTenemosMoneyColor;
    public Vector3 positionOffset;

    //para que nos salga esta opcion en el script
    [HideInInspector]
    public GameObject unidad;
    [HideInInspector]
    public PlanosCos planosCos;
    [HideInInspector]
    public bool esMejorable = false;


    private Renderer rend;
    private Color startColor;

    BuildManager buildManager;

    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;

        buildManager = BuildManager.instance;
    }
    //posicion para construir
    public Vector3 GetBuildPosition()
    {
       return transform.position + positionOffset;
    }
    //control del raton
    void OnMouseDown()
      {
        if (EventSystem.current.IsPointerOverGameObject())
        {
           // Debug.Log("holuuuuuu");
            return;
        }

        if (unidad != null)
        {
            buildManager.SelectCaja(this);
            return;
        }

        if (!buildManager.CanBuild)
        //Debug.Log("holaaaaaaaaaaaaaa");
            return;

        // Debug.Log("hieeeeeeeeeee");

        BuildUnidades(buildManager.GetUnidadToBuild());
    }
    //para construir las unidaddes en las cajas(contenedores)
    void BuildUnidades(PlanosCos planos)
    {
        if (Stats.money < planos.cost)
        {
            Debug.Log("No hay money");
            return;
        }

        Stats.money -= planos.cost;

        GameObject _unidad = (GameObject)Instantiate(planos.prefab, GetBuildPosition(), transform.rotation);
        unidad = _unidad;

        planosCos = planos;

        Debug.Log("Unidad Construida!!!");
    }

    public void MejorarUnidad()
    {
        if (Stats.money < planosCos.mejoracost)
        {
            Debug.Log("No hay money para mejorar");
            return;
        }

        Stats.money -= planosCos.mejoracost;
        //nos quitamos de la vieja 
        Destroy(unidad);
        //ponemos la unidad nueva
        GameObject _unidad = (GameObject)Instantiate(planosCos.mejoraPrefab, GetBuildPosition(), transform.rotation);
        unidad = _unidad;

        esMejorable = true;

        Debug.Log("Unidad Mejorada!!!");
    }

    //control de raton
    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (!buildManager.CanBuild)
            return;

        if (buildManager.HasMoney)
          {
             rend.material.color = hoverColor;
          }
        else
          {
             rend.material.color = noTenemosMoneyColor;
          }

        rend.material.color = hoverColor;
    }
    //para vender la unidad colocada en el tilemap
    public void VentaUnidad()
    {
        Stats.money += planosCos.GetCantidadVenta();
        print(planosCos.GetCantidadVenta());
        print(planosCos);
        Destroy(unidad);
        planosCos = null;

    }
    void OnMouseExit()
    {
        rend.material.color = startColor;
    }
}
