﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orco : MonoBehaviour
{
    //Velocidad de movimiento de la Unidad
    [SerializeField]
    private float velocity = 0.3f;
    public int vida = 50;
    public int daño = 15;
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        //Fijamos velocidad y direccion de la unidad
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        TeMato();
    }
    //Funcion que activa el game over si este enemigo sale de la pantalla por la parte izq.
    private void TeMato()
    {
        if (this.transform.position.x <= -9.5f)
        {
            gameManager.GameOver();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Tanke")
        {
            //Quitamos vida al tanke
            collision.gameObject.GetComponent<Tanke>().vida -= daño;
            
            //Si la vida del tanke llega a 0, muere.
            if (collision.gameObject.GetComponent<Tanke>().vida <= 0)
            {
                Destroy(collision.gameObject);
            }
            //Tanke daña a Orco
            this.vida -= 10;
            //Si vida Orco llega a 0, muere
            if (vida <= 0)
            {
                Stats.money += 40;
                Destroy(this.gameObject);
            }
            //Sufrimos retroceso por empujon Tanke
            this.transform.position = new Vector2(this.transform.position.x + 2, this.transform.position.y);

        }
        if (collision.gameObject.tag == "Arquero")
        {
            //Restamos vida ha Mago
            collision.gameObject.GetComponent<Arquero>().vida -= daño;
            //Si la vida de Mago llega a 0, lo matamos cruelmente
            if (collision.gameObject.GetComponent<Arquero>().vida <= 0)
            {
                Destroy(collision.gameObject);
            }
        }
    }
}
