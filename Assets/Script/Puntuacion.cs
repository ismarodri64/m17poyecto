﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntuacion : MonoBehaviour
{

    public static Puntuacion puntuacion;

    public Text puntuacionText;
    int score = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (puntuacion == null)
        {
            puntuacion = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            puntuacion.puntuacionText = GameObject.Find("Text").GetComponent<Text>();
            Destroy(this);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(puntuacionText != null)
        {
            puntuacionText.text = "Puntuacion:  " + score ;
        }
    }

    public void IncrementarPuntuacion (int s)
    {
        score += s;
        puntuacionText.text = "Puntuacion: " + score;
    }
}
