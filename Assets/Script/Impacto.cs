﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Esta clase es el rango de vision del mago
//Si un enemigo entra en este collaider, el mago empieza a atacar
public class Impacto : MonoBehaviour
{
    //variable que nos dira si podemos disparar o no
    private float puedoDisparar = 0.0f;
    //Tiempo entre disparo y disparo del Mago
    private float velocidadDisparo = 1.3f;
    //Script del Mago(Que antes era un arquero pero...bueno, el Mago!)
    public Arquero padre;
    
    // Start is called before the first frame update
    void Start()
    {
        //Asignamos la script del mago
        padre = GetComponentInParent<Arquero>(); 
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //si un enemigo entra en el radio de vision del Mago y y la velocidad de disparo lo permite,
        //el Mago ataca
        if (collision.gameObject.tag == "Enemigo" && Time.time > puedoDisparar)
        {
            //Debug.Log("colisiono Enemigo1");
            //Llama la funcion Disparar() del padre
            padre.Disparar();
            //Ponemos  coolDown en marcha
            puedoDisparar = Time.time + velocidadDisparo;
        }
        //si un Orco entra en el radio de vision del Mago y y la velocidad de disparo lo permite,
        //el Mago ataca
        else if (collision.gameObject.tag == "Orco" && Time.time > puedoDisparar)
        {
            //Debug.Log("colisiono Orco");
            //Llama la funcion Disparar() del padre
            padre.Disparar();
            //Ponemos  coolDown en marcha
            puedoDisparar = Time.time + velocidadDisparo;
        }
        //si un enemigo volador entra en el radio de vision del Mago y y la velocidad de disparo lo permite,
        //el Mago ataca
        else if (collision.gameObject.tag == "Volador1" && Time.time > puedoDisparar)
        {
            //Debug.Log("colisiono Volador1");
            //Llama la funcion Disparar() del padre
            padre.Disparar();
            //Ponemos  coolDown en marcha
            puedoDisparar = Time.time + velocidadDisparo;
        }
    }
    //Cuando no hay nadie en la vista del mago, desactivamos la animacion de ataque del Mago
    //Esto nos da un problema con sus porpias bolas de hielo... Tengo que repasarlo
    private void OnTriggerExit2D(Collider2D collision)
    {
        padre.anim.SetBool("ataque", false);
    }
}
