﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    //Musica de fondo del juego
    public AudioSource audioFondo;
    //Bool que nos indica el fin del juego
    public bool gameOver = false;

    void Start()
    {
        //Asignamos el AudioSource
        audioFondo = this.GetComponent<AudioSource>();
        //Y le damos play
        audioFondo.Play();
    }
    //Si nos llaman, cambiamos de escena y se acabo
    public void GameOver()
    {
        gameOver = true;
        SceneManager.LoadScene("GameOver");
    }
}
